# README #

This demo can be run by using a free firebase account, and static hosting, such as this bitbucket service,
or github.io service for open source apps.

### testing open food network features via ionic and firebase ###

* hubs
* producers
* farms
* produce
* custoners
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How to run? ###

* download repo - either via zip file download link, or by using git command. ( git clone https://bitbucket.org/scd_hub/ofn_demo )
* cd ofn_demo
* npm i
* make sure ionic is installed - http://ionicframework.com/docs/v1/guide/installation.html
* How to run tests - they are being installed now via nightwatch, connected to gulp test command
* Deployment instructions - harden the login and auth features, security update and review needed still.

### Contribution guidelines ###

* this is a development version, you are welcome to fork this and create your own service from it.
* email me - greg@scdhub.org or make a pull request via bitbucket account here
* Other guidelines. using an IDE like sublime text, idea https://www.jetbrains.com/idea/download or atom helps alot. 

### Who do I talk to? ###

* greg@scdhub.org
* check out the forum and mailing list at OFN

# Ionic Firebase Shopping Cart App
[![Awesome](https://cdn.rawgit.com/sindresorhus/awesome/d7305f38d29fed78fa85652e3a63e154dd8e8829/media/badge.svg)](https://github.com/arjunsk/ionic-firebase-shopping-cart)

### FoodKart V0.3
![FoodKart V0.3 ](/fk-latest.png)

#### Features:
      1. Achieved significant increase in performance and UI using crosswalk and Ionic meterial.
      2. Maintains realtime cart and order tracking with the use of firebase.

#### Use Case Scienario:
      Suppose you and your friend are in food court. You want to buy food from Mc Donald.
      Both of you connect to the Mc Donoalds Wifi.One of you Registers and share the login credentails.
      Now both of you can see the Menu and add the products to the cart in real time.
      On placing the order, both of you can track the Order status.

      No need of long Queue to order your favorate food.


Make sure your read my complete tutorial series on ["Getting Started with Firebase."](http://www.arjunsk.com/tag/firebase/)

#### App Demo
      Use Ionic View to see the demo of the app: http://view.ionic.io/
      View Id: 272277d5

#### Comming Soon ( Ah...not very soon!)
      1. Backend for Processing Orders.
      2. Favourates Page.
      3. Offers Page.
      4. Facebook Login.

Make sure to [follow me on Git ](http://github.com/arjunsk) for getting the Latest Project Updates.

## Donate

If **FoodKart** was helpful for you, send a donation as a thank you. :)

![Bitcoin](/btc.png)

**Bitcoin Adddress**: `1KprXNR7gDHMDUftLnHQWKdpArM2dq3Q5g`

or

**Paypal**:  [paypal.me/arjunsk](https://www.paypal.me/arjunsk/5)


#### Update (15-07-2016):

Video Tutorial:

[![Ionic Firebase Shopping Cart](https://img.youtube.com/vi/GKlnjdbmPxU/0.jpg)](https://www.youtube.com/watch?v=GKlnjdbmPxU&feature=youtu.be)